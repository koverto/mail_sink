use crate::handler::Handler;
use crate::opts::Opts;
use crate::store::MailStore;
use anyhow::{anyhow, Result};
use mailin_embedded;
use std::net::TcpListener;

pub fn from_opts(opts: &Opts) -> Result<mailin_embedded::Server<Handler>> {
    let handler = Handler {
        mailstore: MailStore::new(&opts.maildir()),
    };
    let mut mailin = mailin_embedded::Server::new(handler);
    mailin
        .with_name(opts.domain())
        .with_ssl(opts.ssl_config())
        .map_err(|e| anyhow!("{}", e))?;
    // Bind TCP listener
    let listener = TcpListener::bind(opts.addr())?;
    mailin.with_tcp_listener(listener);
    Ok(mailin)
}
