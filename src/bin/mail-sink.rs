use anyhow::{anyhow, Result};
use getopts::Options;
use mail_sink::logger;
use mail_sink::opts;
use mail_sink::server;
use nix::unistd;
use privdrop::PrivDrop;
use std::env;

fn print_usage(program: &str, opts: &Options) {
    let brief = format!("Usage: {} [options]", program);
    print!("{}", opts.usage(&brief));
}

fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    let opts = opts::Opts::new(&args)?;
    if opts.help_present() {
        print_usage(&args[0], &opts.opts);
        return Ok(());
    }
    let server = server::from_opts(&opts)?;

    // Drop privileges if root
    if unistd::geteuid().is_root() {
        let mut privdrop = PrivDrop::default().user(opts.user());
        if let Some(group) = opts.group() {
            privdrop = privdrop.group(group);
        }
        privdrop.apply()?;
    }

    logger::setup(opts.log_directory())?;

    server.serve().map_err(|e| anyhow!("{}", e))
}
