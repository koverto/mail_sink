use anyhow::{anyhow, Result};
use chrono::Local;
use simplelog::{CombinedLogger, Config, LevelFilter, TermLogger, TerminalMode, WriteLogger};
use std::fs::File;
use std::path::Path;

pub fn setup(log_dir: Option<String>) -> Result<()> {
    let log_level = LevelFilter::Info;
    // Create a trace logger that writes SMTP interaction to file
    if let Some(dir) = log_dir {
        let log_path = Path::new(&dir);
        let datetime = Local::now().format("%Y%m%d%H%M%S").to_string();
        let filename = format!("smtp-{}.log", datetime);
        let filepath = log_path.join(&filename);
        let file = File::create(&filepath)?;
        CombinedLogger::init(vec![
            TermLogger::new(log_level, Config::default(), TerminalMode::Stdout),
            WriteLogger::new(LevelFilter::Trace, Config::default(), file),
        ])
        .map_err(|err| anyhow!("Cannot initialize logger: {}", err))
    } else {
        CombinedLogger::init(vec![TermLogger::new(
            log_level,
            Config::default(),
            TerminalMode::Stdout,
        )])
        .map_err(|err| anyhow!("Cannot initialize logger: {}", err))
    }
}
