use anyhow::{anyhow, Result};
use getopts;
use mailin_embedded::SslConfig;
use std::string::String;

const DOMAIN: &str = "localhost";
const DEFAULT_ADDRESS: &str = "127.0.0.1:8025";
const DEFAULT_USER: &str = "nobody";

// Command line option names
const OPT_HELP: &str = "help";
const OPT_ADDRESS: &str = "address";
const OPT_LOG: &str = "log";
const OPT_SERVER: &str = "server";
const OPT_SSL_CERT: &str = "ssl-cert";
const OPT_SSL_KEY: &str = "ssl-key";
const OPT_SSL_CHAIN: &str = "ssl-chain";
const OPT_USER: &str = "user";
const OPT_GROUP: &str = "group";
const OPT_MAILDIR: &str = "maildir";

pub struct Opts {
    matches: getopts::Matches,
    pub opts: getopts::Options,
}

impl Opts {
    pub fn new(args: &Vec<String>) -> Result<Self> {
        let mut opts = getopts::Options::new();
        opts.optflag("h", OPT_HELP, "print this help menu");
        opts.optopt("a", OPT_ADDRESS, "the address to listen on", "ADDRESS");
        opts.optopt("l", OPT_LOG, "the directory to write logs to", "LOG_DIR");
        opts.optopt("s", OPT_SERVER, "the name of the mailserver", "SERVER");
        opts.optopt("", OPT_SSL_CERT, "ssl certificate", "PEM_FILE");
        opts.optopt("", OPT_SSL_KEY, "ssl certificate key", "PEM_FILE");
        opts.optopt(
            "",
            OPT_SSL_CHAIN,
            "ssl chain of trust for the certificate",
            "PEM_FILE",
        );
        opts.optopt("", OPT_USER, "user to run as", "USER");
        opts.optopt("", OPT_GROUP, "group to run as", "GROUP");
        opts.optopt("", OPT_MAILDIR, "the directory to store mail in", "MAILDIR");
        let matches = opts
            .parse(&args[1..])
            .map_err(|err| anyhow!("Error parsing command line: {}", err))?;
        Ok(Opts { matches, opts })
    }

    pub fn help_present(&self) -> bool {
        self.matches.opt_present(OPT_HELP)
    }

    pub fn ssl_config(&self) -> SslConfig {
        let matches = &self.matches;
        match (matches.opt_str(OPT_SSL_CERT), matches.opt_str(OPT_SSL_KEY)) {
            (Some(cert_path), Some(key_path)) => SslConfig::SelfSigned {
                cert_path,
                key_path,
            },
            (_, _) => SslConfig::None,
        }
    }

    pub fn domain(&self) -> String {
        self.matches
            .opt_str(OPT_SERVER)
            .unwrap_or_else(|| DOMAIN.to_owned())
    }

    pub fn maildir(&self) -> String {
        self.matches
            .opt_str(OPT_MAILDIR)
            .unwrap_or_else(|| "mail".to_owned())
    }

    pub fn addr(&self) -> String {
        self.matches
            .opt_str(OPT_ADDRESS)
            .unwrap_or_else(|| DEFAULT_ADDRESS.to_owned())
    }

    pub fn user(&self) -> String {
        self.matches
            .opt_str(OPT_USER)
            .unwrap_or_else(|| DEFAULT_USER.to_owned())
    }

    pub fn group(&self) -> Option<String> {
        self.matches.opt_str(OPT_GROUP)
    }

    pub fn log_directory(&self) -> Option<String> {
        self.matches.opt_str(OPT_LOG)
    }
}
