use crate::store::MailStore;
use mailin_embedded::{DataResult, HeloResult};
use std::io;
use std::io::Write;
use std::net::IpAddr;

#[derive(Clone)]
pub struct Handler {
    pub mailstore: MailStore,
}

impl mailin_embedded::Handler for Handler {
    fn helo(&mut self, _ip: IpAddr, _domain: &str) -> HeloResult {
        HeloResult::Ok
    }

    fn data_start(
        &mut self,
        _domain: &str,
        _from: &str,
        _is8bit: bool,
        _to: &[String],
    ) -> DataResult {
        match self.mailstore.start_message() {
            Ok(()) => DataResult::Ok,
            Err(_) => DataResult::InternalError,
        }
    }

    fn data(&mut self, buf: &[u8]) -> io::Result<()> {
        self.mailstore.write_all(buf)
    }

    fn data_end(&mut self) -> DataResult {
        match self.mailstore.end_message() {
            Ok(()) => DataResult::Ok,
            Err(_) => DataResult::InternalError,
        }
    }
}
