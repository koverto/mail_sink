# Mail-Sink

A simple smtp server to receive and store mails.

mail-sink is similar to smtp-sink but does not require installing postgres.
It's based on [mailin-server] - an example SMTP server for the Mailin library.

## Subdirectories

The [systemd](systemd/README.md) directory contains unit files for use with Linux systems that use systemd.

The [scripts](scripts) directory contains useful scripts for playing with the server.

[mailin-server]: https://gitlab.com/alienscience/mailin
